package main

import (
	"fmt"
	"math/rand"
	"time"
)

func sleep() {
	time.Sleep(
		time.Duration(rand.Intn(3000)) * 1000,
	)
}

func producer(ch chan<- int) {
	for {
		sleep() // sleep some random time

		n := rand.Intn(100)

		fmt.Printf("Producer -> %d\n", n)
		ch <- n
	}
}

func consumer(ch <-chan int, name string) {
	for n := range ch {
		fmt.Printf("%s <- %d\n", name, n)
	}
}

func fanOut(chProducer <-chan int, chEven, chOdd chan<- int) {
	for n := range chProducer {
		if n%2 == 0 {
			chEven <- n
		} else {
			chOdd <- n
		}
	}
}

func main() {
	chEven := make(chan int)
	chOdd := make(chan int)
	chProducer := make(chan int)

	go consumer(chEven, "Even consumer")
	go consumer(chOdd, "Odd consumer")
	go producer(chProducer)

	fanOut(chProducer, chEven, chOdd)
}
