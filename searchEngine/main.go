package main

import (
	"log"
	"os"
	"strings"
	"time"
)

type User struct {
	Email, Username string
}

var Database = []User{
	{Email: "uesteibar@gmail.com", Username: "uesteibar"},
	{Email: "example@hola.com", Username: "example"},
	{Email: "hola@hola.com", Username: "hola"},
	{Email: "patata@hola.com", Username: "patata"},
	{Email: "titi@hola.com", Username: "titi"},
}

type Worker struct {
	users []User
	ch    chan *User
}

func NewWorker(users []User, ch chan *User) *Worker {
	return &Worker{users: users, ch: ch}
}

func (w *Worker) Find(email string) {
	for i := range w.users {
		user := &w.users[i]

		if strings.Contains(user.Email, email) {
			w.ch <- user
		}
	}
}

func main() {
	email := os.Args[1]

	log.Printf("Looking for %s...", email)

	ch := make(chan *User)

	go NewWorker(Database[:3], ch).Find(email)
	go NewWorker(Database[3:], ch).Find(email)

	for {
		select {
		case user := <-ch:
			log.Printf("Found user %s with email %s", user.Username, user.Email)
		case <-time.After(2 * time.Second):
			return
		}
	}
}
