package arithmetic

func Sum(args ...int) int {
	res := 0
	for _, arg := range args {
		res += arg
	}

	return res
}
