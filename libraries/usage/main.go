package main

import (
	"fmt"
	"gitlab.com/uesteibar/learning-go/libraries/arithmetic"
)

func main() {
	res := arithmetic.Sum(1, 3)
	fmt.Printf("Result is %d\n", res)

}
