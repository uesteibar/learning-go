package main

import (
	"fmt"
)

type Tree struct {
	node *Node
}

func (t *Tree) Insert(v int) *Tree {
	if t.node == nil {
		t.node = &Node{value: v}
	} else {
		t.node.Insert(v)
	}

	return t
}

func (t Tree) Exists(v int) bool {
	fmt.Println("Looking for:", v)

	if t.node == nil {
		return false
	} else if t.node.value == v {
		return true
	} else {
		return t.node.Exists(v)
	}

}

type Node struct {
	left, right *Node
	value       int
}

func (n *Node) Insert(v int) {
	if v <= n.value {
		if n.left == nil {
			n.left = &Node{value: v}
		} else {
			n.left.Insert(v)
		}
	} else {
		if n.right == nil {
			n.right = &Node{value: v}
		} else {
			n.right.Insert(v)
		}
	}
}

func (n *Node) Exists(v int) bool {
	fmt.Println("Value:", n.value)

	if n.value == v {
		return true
	} else if v < n.value {
		if n.left == nil {
			return false
		} else {
			return n.left.Exists(v)
		}
	} else {
		if n.right == nil {
			return false
		} else {
			return n.right.Exists(v)
		}
	}
}

func printNode(n *Node) {
	if n == nil {
		return
	}

	fmt.Println(n.value)
	printNode(n.left)
	printNode(n.right)

}

func main() {
	t := Tree{}
	t.Insert(1).
		Insert(5).
		Insert(3).
		Insert(9).
		Insert(3).
		Insert(4).
		Insert(5).
		Insert(8).
		Insert(7)

	fmt.Println(t.Exists(1))
	fmt.Println(t.Exists(5))
	fmt.Println(t.Exists(3))
	fmt.Println(t.Exists(9))
	fmt.Println(t.Exists(3))
	fmt.Println(t.Exists(4))
	fmt.Println(t.Exists(5))
	fmt.Println(t.Exists(8))
	fmt.Println(t.Exists(7))
	fmt.Println(t.Exists(2))

	printNode(t.node)
}
