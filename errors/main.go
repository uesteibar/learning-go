package main

import (
	"errors"
	"fmt"
)

type User struct {
	Role string
}

func (u User) Salary() (int, error) {
	if u.Role == "" {
		return 0, errors.New("There's no role!")
	}

	switch u.Role {
	case "Developer":
		return 100, nil
	case "Architect":
		return 200, nil
	}
	return 0, errors.New(
		fmt.Sprintf("Unknown role: %s", u.Role),
	)
}

func handle(role string) {
	user := User{Role: role}

	if salary, err := user.Salary(); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Salary:", salary)
	}
}

func main() {
	handle("Developer")
	handle("Designer")
	handle("")
}
