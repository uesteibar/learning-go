package main

func hello() {
	println("Hello")
}

func sum(x, y int) int {
	return x + y
}

func main() {
	hello()
	println(sum(1, 2))

	swap := func(x, y int) (int, int) { return y, x }

	a, b := swap(10, 20)
	println(a)
	println(b)
}
