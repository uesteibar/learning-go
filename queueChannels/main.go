package main

import "fmt"

type Queue struct {
	items chan interface{}
}

func (q *Queue) Enqueue(i interface{}) {
	q.items <- i
}

func (q *Queue) Pop() interface{} {
	return <-q.items
}

func main() {
	q := Queue{items: make(chan interface{}, 16)}

	q.Enqueue(1)
	q.Enqueue(2)
	q.Enqueue(3)

	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
}
