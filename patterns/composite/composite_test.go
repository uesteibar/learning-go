package composite

import (
	"testing"
)

func TestCompositeA(t *testing.T) {
	localSwim := Swim
	swimmer := CompositeSwimmerA{
		Swim: &localSwim,
	}
	swimmer.Athlete.Train()
	(*swimmer.Swim)()
}

func TestAnimal(t *testing.T) {
	shark := Shark{
		Swim: Swim,
	}

	shark.Eat()
	shark.Swim()
}

func TestCompositeB(t *testing.T) {
	swimmer := CompositeSwimmerB{
		&Athlete{},
		&SwimmerImplementor{},
	}

	swimmer.Swim()
	swimmer.Train()
}
