package prototype

import (
	"errors"
)

type ShirtCloner interface {
	GetClone(shirtType int) (*Shirt, error)
}

const (
	White = 1
)

type ShirtsCache struct{}

func (s *ShirtsCache) GetClone(shirtType int) (*Shirt, error) {
	switch shirtType {
	case White:
		newItem := *whitePrototype
		return &newItem, nil
	default:
		return nil, errors.New("Not implemented")
	}
}

type ItemInfoGetter interface {
	GetInfo() string
}

type ShirtColor byte
type Shirt struct {
	Price float32
	Color ShirtColor
}

func GetShirtsCloner() ShirtCloner {
	return &ShirtsCache{}
}

var whitePrototype *Shirt = &Shirt{
	Price: 15.00,
	Color: White,
}

func (s *Shirt) GetPrice() float32 {
	return s.Price
}
