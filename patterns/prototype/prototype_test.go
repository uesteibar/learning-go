package prototype

import (
	"testing"
)

func TestShirtCloner(t *testing.T) {
	shirtCache := GetShirtsCloner()

	if shirtCache == nil {
		t.Fatal("Expected GetShirtsCloner() to return a ShirtsCache")
	}

	item, err := shirtCache.GetClone(White)
	if err != nil {
		t.Error(err)
	}
	if item == whitePrototype {
		t.Error("Actual shirt should be different from whitePrototype")
	}

	item2, err := shirtCache.GetClone(White)
	if err != nil {
		t.Error(err)
	}
	if item2 == whitePrototype {
		t.Error("Actual shirt should be different from whitePrototype")
	}

	if &item == &item2 {
		t.Errorf("Memory positions for clones should be different. Got %p and %p", &item, &item2)
	}
}
