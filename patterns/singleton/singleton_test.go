package singleton

import (
	"testing"
)

func TestGetInstance(t *testing.T) {
	counter := GetInstance()

	if counter == nil {
		t.Error("expected singleton instance after calling GetInstance(), got nil")
	}

	expectedCounter := counter

	currentCount := counter.AddOne()
	if currentCount != 1 {
		t.Errorf("After calling AddOne() for the first time, expected 1, got %d\n", currentCount)
	}

	if GetInstance() != expectedCounter {
		t.Error("After running GetInstance(), expected to get the same instance but got a new one")
	}
}
