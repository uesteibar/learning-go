package builder

type BuildProcess interface {
	SetWheels() BuildProcess
	SetSeats() BuildProcess
	SetStructure() BuildProcess
	Build() Vehicle
}

type Director struct {
	builder BuildProcess
}

func (d *Director) Construct() Vehicle {
	return d.builder.SetWheels().SetSeats().SetStructure().Build()
}

func (d *Director) SetBuilder(b BuildProcess) {
	d.builder = b
}

type Vehicle struct {
	Wheels    int
	Seats     int
	Structure string
}

type Builder struct {
	v Vehicle
}

func (b *Builder) SetWheels() BuildProcess {
	b.v.Wheels = 4
	return b
}

func (b *Builder) SetSeats() BuildProcess {
	b.v.Seats = 2
	return b
}
func (b *Builder) SetStructure() BuildProcess {
	b.v.Structure = "sport"
	return b
}

func (b *Builder) Build() Vehicle {
	return b.v
}
