package builder

import "testing"

func TestBuilder(t *testing.T) {
	b := &Builder{}
	d := &Director{}
	d.SetBuilder(b)

	v := d.Construct()

	if v.Wheels != 4 {
		t.Errorf("Expected vehicle to have 4 wheels, instead found %d\n", v.Wheels)
	}
	if v.Seats != 2 {
		t.Errorf("Expected vehicle to have 5 seats, instead found %d\n", v.Seats)
	}
	if v.Structure != "sport" {
		t.Errorf("Expected vehicle to have structure 'sport', instead found %s\n", v.Structure)
	}
}
