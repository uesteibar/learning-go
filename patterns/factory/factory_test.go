package factory

import (
	"strings"
	"testing"
)

func TestPaymentMethodCash(t *testing.T) {
	pm, err := GetPaymentMethod(Cash)

	if err != nil {
		t.Fatalf("Error: %s", err)
	}

	msg := pm.Pay(5.40)

	if !strings.Contains(msg, "payed_using_cash") {
		t.Errorf("The payment didn't go through , received response: %s", msg)
	}
}

func TestPaymentMethodDebitCard(t *testing.T) {
	pm, err := GetPaymentMethod(DebitCard)

	if err != nil {
		t.Fatalf("Error: %s", err)
	}

	msg := pm.Pay(10.15)

	if !strings.Contains(msg, "payed_using_debit_card") {
		t.Errorf("The payment didn't go through , received response: %s", msg)
	}
}

func TestPaymentMethodNonExistent(t *testing.T) {
	_, err := GetPaymentMethod(3)

	if err == nil {
		t.Fatal("A non existent payment method should return an error")
	}
}
