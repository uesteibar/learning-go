package factory

import (
	"errors"
)

type PaymentMethod interface {
	Pay(amount float32) string
}

const (
	Cash      = 1
	DebitCard = 2
)

func GetPaymentMethod(paymentMethodCode int) (PaymentMethod, error) {
	switch paymentMethodCode {
	case Cash:
		return &CashPM{}, nil
	case DebitCard:
		return &DebitCardPM{}, nil
	default:
		return nil, errors.New("Not implemented")
	}
}

type CashPM struct{}

func (c *CashPM) Pay(amount float32) string {
	return "payed_using_cash"
}

type DebitCardPM struct{}

func (d *DebitCardPM) Pay(amount float32) string {
	return "payed_using_debit_card"
}
