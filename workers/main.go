package main

import (
	"fmt"
	"math/rand"
	"time"
)

func echoWorker(in, out chan int) {
	for n := range in {
		time.Sleep(
			time.Duration(rand.Intn(3000)) * time.Millisecond,
		)

		out <- n
	}
}

func produce(ch chan<- int) {
	i := 0
	for {
		fmt.Printf("-> Send job: %d\n", i)
		ch <- i

		i++
	}
}

func consume(ch <-chan int) {
	for i := range ch {
		fmt.Printf("<- Finished with job: %d\n", i)
	}
}

func main() {
	in := make(chan int)
	out := make(chan int)

	for i := 0; i < 100; i++ {
		go echoWorker(in, out)
	}
	go produce(in)

	consume(out)
}
