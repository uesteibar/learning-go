package main

func sum(x int, y int) int {
	return x + y
}

func main() {
	x := 10
	y := 11
	// y := 11.5 can't do because different types

	sum(x, y)
}
