package main

import "fmt"

func msg(name string) string {
	return "Hello " + name
}

func greet(name string) {
	var msg string = msg(name)

	fmt.Println(msg)
}

func main() {
	greet("Unai")
	greet("Gophers")
}
