package main

import "fmt"

type User struct {
	Name, Role string
	Age        int
}

func (u User) Salary() int {
	switch u.Role {
	case "Developer":
		return 100
	case "Architect":
		return 200
	}
	return 0
}

func (u *User) Promote() {
	u.Role = "Architect"
}

func main() {
	unai := User{Name: "Unai", Role: "Developer", Age: 27}

	fmt.Println(unai)
	fmt.Println(unai.Role)
	fmt.Println(unai.Salary())

	unai.Promote()
	fmt.Println(unai)
}
