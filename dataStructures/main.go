package main

import "fmt"

func print(parts ...interface{}) {
	fmt.Println(parts...)
}

func slices() {
	numbers := []int{1, 7, 4} // typed slice

	print(numbers)
	print(numbers[0])
	print(len(numbers))
	print(numbers[len(numbers)-1])
	print(append(numbers, 100))

	for index, value := range numbers {
		print("Index:", index, "Value:", value)
	}
}

func printAge(user map[string]string) {
	age, ok := user["age"]
	if ok == true {
		print("Age:", age)
	} else {
		print("Age not found")
	}
}

func maps() {
	user := map[string]string{
		"name":  "Unai",
		"email": "uesteibar@example.com",
		"role":  "Developer",
	}

	print(user)
	print(user["email"])

	printAge(user)

	user["age"] = "30"
	printAge(user)
}

func main() {
	slices()
	maps()
}
