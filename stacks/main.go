package main

import (
	"errors"
	"fmt"
)

type Stack struct {
	items []interface{}
}

func (s *Stack) Push(item interface{}) {
	s.items = append(s.items, item)
}

func (s *Stack) Pop() (interface{}, error) {
	size := len(s.items)
	if size == 0 {
		return -1, errors.New("Can't pop from an empty stack")
	}

	item, items := s.items[size-1], s.items[0:size-1]
	s.items = items

	return item, nil
}

type Node struct {
	value interface{}
	next  *Node
	prev  *Node
}

func main() {
	s := Stack{}
	s.Push(1)
	s.Push(2)
	s.Push("Test")

	n, _ := s.Pop()
	fmt.Println(n)

	n, _ = s.Pop()
	fmt.Println(n)

	n, _ = s.Pop()
	fmt.Println(n)

	_, err := s.Pop()
	fmt.Println(err)
}
