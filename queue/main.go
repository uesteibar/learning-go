package main

import "fmt"

type Queue struct {
	items []interface{}
}

func (q *Queue) Enqueue(i interface{}) {
	q.items = append(q.items, i)
}

func (q *Queue) Pop() interface{} {
	if len(q.items) == 0 {
		return -1
	}

	item, items := q.items[0], q.items[1:]
	q.items = items

	return item
}

func main() {
	q := Queue{}
	q.Enqueue(1)
	q.Enqueue(2)
	q.Enqueue(3)

	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
	fmt.Println(q.Pop())
}
