package main

func main() {
	forLoop()
	ifCondition(true)
	ifCondition(false)
}

func swithCase(id string) {
	switch id {
	case "apple", "tomatoes":
		println(10)
	case "carrot":
		println(20)
	default:
		println(0)
	}
}

func ifCondition(cond bool) {
	if cond == false {
		println("False")
	} else {
		println("True")
	}
}

func forLoop() {
	for i := 0; i < 10; i++ {
		println(i)
	}
}
