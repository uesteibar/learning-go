package main

import "errors"

type List struct {
	head *Node
	tail *Node
}

func (l List) First() *Node {
	return l.head
}

func (l List) Last() *Node {
	return l.tail
}

func (l List) Find(v int) (*Node, error) {
	n := l.First()

	for {
		if n == nil || n.value == v {
			break
		}

		n = n.next
	}

	if n == nil {
		return nil, errors.New("Couldn't find the value")
	}

	return n, nil
}

func (l *List) Push(v int) {
	node := &Node{value: v}

	if l.head == nil {
		l.head = node
	} else {
		l.tail.next = node
		node.prev = l.tail
	}

	l.tail = node
}

type Node struct {
	value int
	next  *Node
	prev  *Node
}

func (n Node) Next() *Node {
	return n.next
}

func find(l List, v int) {
	if n, err := l.Find(v); err != nil {
		println("Didn't find", v)
	} else {
		println("Found", n.value)
	}
}

func main() {
	l := List{}
	l.Push(1)
	l.Push(2)
	l.Push(3)

	n := l.First()

	for {
		println(n.value)
		if n = n.next; n == nil {
			break
		}
	}

	n = l.Last()
	for {
		println(n.value)
		if n = n.prev; n == nil {
			break
		}
	}

	find(l, 1)
	find(l, 4)
}
